import React, {useState} from 'react';
import AddTodo from "./todos/add/AddTodo";
import TodoList from "./todos/list/TodoList";
import {Todo} from "./todos/todo";
import {Filter} from "./todos/filter";
import Footer from "./todos/footer/Footer";
import useTodosStorage from "./todos/useTodosStorage";
import {v4 as uuidv4} from 'uuid';

const App = () => {

    const {todos, setTodos} = useTodosStorage([]);
    const [filter, setFilter] = useState(Filter.ALL);

    const removeTodo = (todo: Todo): void => {
        setTodos(todos.filter(t => t.id !== todo.id));
    }

    const updateTodo = (todo: Todo, title?: string, completed?: boolean): void => {
        const index = todos.findIndex(t => t.id === todo.id);
        todos[index] = {
            ...todos[index],
            title: title || todos[index].title,
            completed: completed || todos[index].completed
        };
        setTodos([...todos]);
    }

    const addTodo = (title: string): void => {
        const todo = {id: uuidv4(), title: title, completed: false, order: todos.length + 1};
        setTodos([...todos, todo]);
    }

    const clearCompleted = (): void => {
        setTodos(todos.filter(t => !t.completed));
    }

    const updateFilter = (filter: Filter): void => {
        setFilter(filter);
    }

    const filteredTodos = todos.filter((todo) => {
        switch (filter) {
            case Filter.ACTIVE:
                return !todo.completed;
            case Filter.COMPLETED:
                return todo.completed;
            default:
                return true;
        }
    });

    return (
        <section className="todoapp">
            <header className="header">
                <h1>todos</h1>
                <AddTodo onAddTodo={(title: string) => addTodo(title)}/>
            </header>
            <section className="main">
                <input id="toggle-all" className="toggle-all" type="checkbox"/>
                <label htmlFor="toggle-all">Mark all as complete</label>
                <TodoList todos={filteredTodos} onRemove={(todo: Todo) => removeTodo(todo)}
                          onUpdate={(todo: Todo, title?: string, completed?: boolean) => updateTodo(todo, title, completed)}
                />
            </section>
            <Footer activeCount={todos.filter(todo => !todo.completed).length}
                    onClearCompleted={clearCompleted}
                    onUpdateFilter={(filter: Filter) => updateFilter(filter)}/>
        </section>
    );
}

export default App;
