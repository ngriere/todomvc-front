import TodoItem from "../item/TodoItem";
import React from "react";
import {Todo, Todos} from "../todo";

type TodoListProps = {
    todos: Todos;
    onRemove: (todo: Todo) => void;
    onUpdate: (todo: Todo, title?: string, completed?: boolean) => void;
};

const TodoList = ({todos, onRemove, onUpdate}: TodoListProps) => {
    return (
        <ul className="todo-list">
            {todos.length > 0 && todos.map((todo) => <TodoItem title={todo.title} completed={todo.completed}
                                                               key={todo.id}
                                                               onRemove={() => onRemove(todo)}
                                                               onUpdate={(fields: { title?: string, completed?: boolean }) => onUpdate(todo, fields.title, fields.completed)}/>)}
        </ul>
    );
}

export default TodoList;