import React from "react";
import {render, screen} from "@testing-library/react";
import TodoList from "./TodoList";
import {v4 as uuidv4} from "uuid";

describe('TodoList component', () => {

    it('should render properly if at lest one todo', async () => {
        // GIVEN
        const todos = [
            {
                id: uuidv4(),
                title: "Buy Unicorn",
                completed: false,
                order: 1
            },
            {
                id: uuidv4(),
                title: "Do Laundry",
                completed: false,
                order: 2
            }
        ];
        const {asFragment} = render(<TodoList todos={todos} onRemove={jest.fn()} onUpdate={jest.fn()}/>);
        const todoListElement = await screen.findByRole("list");

        // THEN
        expect(asFragment()).toMatchSnapshot();
        expect(todoListElement).toBeInTheDocument();
        expect(todoListElement.className).toEqual("todo-list");
        expect(todoListElement.childElementCount).toEqual(2);
    });

    it('should render properly if no todos', async () => {
        // GIVEN
        const {asFragment} = render(<TodoList todos={[]} onRemove={jest.fn()} onUpdate={jest.fn()}/>);
        const todoListElement = await screen.findByRole("list");

        // THEN
        expect(asFragment()).toMatchSnapshot();
        expect(todoListElement).toBeInTheDocument();
        expect(todoListElement.className).toEqual("todo-list");
        expect(todoListElement.childElementCount).toEqual(0);
    });
});