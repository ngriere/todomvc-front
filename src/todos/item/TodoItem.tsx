import React, {ChangeEvent, KeyboardEvent, useState} from "react";
import './todo.css'

type TodoItemProps = {
    title: string;
    completed: boolean;
    onRemove: () => void;
    onUpdate: (fields: { title?: string; completed?: boolean }) => void;
};

const TodoItem = ({onRemove, onUpdate, title, completed}: TodoItemProps) => {

    const [viewMode, setViewMode] = useState(true);
    const [editedTitle, setEditedTitle] = useState(title);

    const handleToggleComplete = () => onUpdate({completed: !completed});

    const handleKeyUp = (event: KeyboardEvent) => {
        if (event.key === "Enter" && editedTitle) {
            submitEdit();
        }

        if (event.key === "Escape") {
            setEditedTitle("");
        }
    }

    const submitEdit = () => {
        const cleanEditedTitle = editedTitle.trim();
        cleanEditedTitle === "" ? onRemove() : onUpdate({title: cleanEditedTitle});
        setViewMode(true);
    }

    const handleEditTitle = (event: ChangeEvent<HTMLInputElement>) => {
        setEditedTitle(event.target.value);
    }

    return (
        <li className={`${completed ? "completed" : ""}${viewMode ? "" : "editing"}`}>
            {viewMode && <div className="view">
                <input id="todo-toggle"
                       name="todo-toggle"
                       className="toggle"
                       type="checkbox"
                       role="checkbox"
                       defaultChecked={completed}
                       onChange={handleToggleComplete}/>
                <label htmlFor="todo-toggle"
                       onClick={(e) => e.preventDefault()}
                       onDoubleClick={() => setViewMode(false)}>
                    {title}
                </label>
                <button className="destroy" onClick={() => onRemove()}/>
            </div>
            }
            {!viewMode && (<input className="edit"
                                  autoFocus
                                  value={editedTitle}
                                  onChange={handleEditTitle}
                                  onKeyUp={handleKeyUp}
                                  onBlur={submitEdit}/>)}
        </li>
    );
}

export default TodoItem;