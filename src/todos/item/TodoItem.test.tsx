import React from 'react';
import {render, screen} from '@testing-library/react';
import TodoItem from "./TodoItem";
import userEvent from "@testing-library/user-event";

describe('TodoItem component', () => {


    it('should render properly', async () => {
        // GIVEN
        const {asFragment} = render(<TodoItem title="Do Laundry" completed={true}
                                              onRemove={jest.fn()}
                                              onUpdate={jest.fn()}/>);
        const input = await screen.findByRole("listitem");

        // THEN
        expect(asFragment()).toMatchSnapshot();
        expect(input).toBeInTheDocument();
    });

    it.each`
  completed    | expected
  ${true} | ${false}
  ${false} | ${true}
`('should notify parent when check $completed', async ({completed, expected}) => {
        // GIVEN
        const onUpdate = jest.fn();
        render(<TodoItem title="Do Laundry" completed={completed}
                         onRemove={jest.fn()}
                         onUpdate={onUpdate}/>);

        const checkboxElement: HTMLInputElement = await screen.findByRole('checkbox');
        expect(checkboxElement).toBeInTheDocument();

        // WHEN
        userEvent.click(checkboxElement);

        // THEN
        expect(onUpdate).toHaveBeenCalledTimes(1);
        expect(onUpdate).toHaveBeenCalledWith({completed: expected});
    });

    it('should toggle todo from active to complete', async () => {
        // GIVEN
        const onUpdate = jest.fn();
        render(<TodoItem title="Do Laundry" completed={false}
                         onRemove={jest.fn()}
                         onUpdate={onUpdate}/>);

        const checkboxElement: HTMLInputElement = await screen.findByRole('checkbox');
        expect(checkboxElement).toBeInTheDocument();
        expect(checkboxElement).not.toBeChecked();

        // WHEN
        userEvent.click(checkboxElement);

        // THEN
        expect(checkboxElement).toBeChecked();
    });

    it('should toggle todo from complete to active', async () => {
        // GIVEN
        const onUpdate = jest.fn();
        render(<TodoItem title="Do Laundry" completed={true}
                         onRemove={jest.fn()}
                         onUpdate={onUpdate}/>);

        const checkboxElement: HTMLInputElement = await screen.findByRole('checkbox');
        expect(checkboxElement).toBeInTheDocument();
        expect(checkboxElement).toBeChecked();

        // WHEN
        userEvent.click(checkboxElement);

        // THEN
        expect(checkboxElement).not.toBeChecked();
    });

    it('should enter edit mode when todo title double-clicked', async () => {
        // GIVEN
        render(<TodoItem title="Do Laundry" completed={false}
                         onRemove={jest.fn()}
                         onUpdate={jest.fn()}/>);

        const title: HTMLLabelElement = await screen.findByText("Do Laundry");
        const checkboxElement: HTMLInputElement = await screen.findByRole('checkbox');
        expect(checkboxElement).toBeInTheDocument();
        expect(title).toBeInTheDocument();

        // WHEN
        userEvent.dblClick(title);

        // THEN
        const editTitleInput = await screen.findByRole('textbox');
        expect(checkboxElement).not.toBeInTheDocument();
        expect(editTitleInput).toBeInTheDocument();
        expect(editTitleInput).toHaveFocus();
    });

    it('should notify parent with edited text when Enter is pressed and go back to view mode', async () => {
        // GIVEN
        const onUpdateSpy = jest.fn();
        render(<TodoItem title="Do Laundry" completed={false}
                         onRemove={jest.fn()}
                         onUpdate={onUpdateSpy}/>);

        const title: HTMLLabelElement = await screen.findByText("Do Laundry");
        userEvent.dblClick(title);
        const editTitleInput = await screen.findByRole('textbox');
        expect(editTitleInput).toHaveFocus();

        // WHEN
        userEvent.type(editTitleInput, " and Buy Unicorn{enter}");

        // THEN
        expect(await screen.findByRole('checkbox')).toBeInTheDocument();
        expect(editTitleInput).not.toBeInTheDocument();
        expect(onUpdateSpy).toHaveBeenCalledTimes(1);
        expect(onUpdateSpy).toHaveBeenCalledWith({title: "Do Laundry and Buy Unicorn"});
    });

    it('should preserve edit state but discard changes when escape is pressed during the edit', async() => {
        // GIVEN
        const onUpdateSpy = jest.fn();
        render(<TodoItem title="Do Laundry" completed={false}
                         onRemove={jest.fn()}
                         onUpdate={onUpdateSpy}/>);

        const title: HTMLLabelElement = await screen.findByText("Do Laundry");
        userEvent.dblClick(title);
        const editTitleInput = await screen.findByRole('textbox');
        userEvent.type(editTitleInput, " and change");
        expect(editTitleInput).toBeInTheDocument();
        expect(editTitleInput).toHaveValue("Do Laundry and change");

        // WHEN
        userEvent.type(editTitleInput, "{esc}");

        // THEN
        expect(editTitleInput).toBeInTheDocument();
        expect(editTitleInput).toHaveValue("");
        expect(onUpdateSpy).not.toHaveBeenCalled();
    })

    it('should notify parent of todo deletion when edit emptied and blurred', async () => {
        // GIVEN
        const onRemoveSpy = jest.fn();
        const onUpdateSpy = jest.fn();
        render(<TodoItem title="Do Laundry" completed={false}
                         onRemove={onRemoveSpy}
                         onUpdate={onUpdateSpy}/>);

        const title: HTMLLabelElement = await screen.findByText("Do Laundry");
        userEvent.dblClick(title);
        const editTitleInput = await screen.findByRole('textbox');
        expect(editTitleInput).toHaveFocus();

        // WHEN
        userEvent.type(editTitleInput, '{esc}');
        userEvent.tab();

        // THEN
        expect(onUpdateSpy).not.toHaveBeenCalled();
        expect(onRemoveSpy).toHaveBeenCalled();
    });

    it('should notify parent of todo deletion when click on delete button', async () => {
        // GIVEN
        const onRemoveSpy = jest.fn();
        render(<TodoItem title="Do Laundry" completed={false}
                         onRemove={onRemoveSpy}
                         onUpdate={jest.fn()}/>);
        const deleteButton: HTMLButtonElement = await screen.findByRole('button');

        // WHEN
        userEvent.click(deleteButton);

        // THEN
        expect(onRemoveSpy).toHaveBeenCalledTimes(1);
        expect(onRemoveSpy).toHaveBeenCalledWith();
    });
});