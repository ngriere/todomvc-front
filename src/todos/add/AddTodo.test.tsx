import React from 'react';
import {render, screen} from '@testing-library/react';
import AddTodo from "./AddTodo";
import userEvent from "@testing-library/user-event";

describe('AddTodo component', () => {

    it('should render properly', async () => {
        // GIVEN
        const {asFragment} = render(<AddTodo onAddTodo={jest.fn()}/>);
        const inputElement = await screen.findByRole("textbox");

        // THEN
        expect(asFragment()).toMatchSnapshot();
        expect(inputElement).toBeInTheDocument();
        expect(inputElement).toHaveFocus();

    });

    it('should handle title change', async () => {
        // GIVEN
        render(<AddTodo onAddTodo={jest.fn()}/>);
        const inputElement = await screen.findByRole("textbox");

        // WHEN
        userEvent.type(inputElement, "Buy Unicorn");

        // THEN
        expect(inputElement).toHaveValue("Buy Unicorn");
    });

    it('should not notify parent when edited title is empty', async () => {
        // GIVEN
        const onAddTodoSpy = jest.fn();
        render(<AddTodo onAddTodo={onAddTodoSpy}/>);
        const inputElement = await screen.findByRole("textbox");

        // WHEN
        userEvent.type(inputElement, '{enter}');

        // THEN
        expect(inputElement).toHaveValue('');
        expect(onAddTodoSpy).not.toHaveBeenCalled();
    });

    it('should not notify parent when edited title is blank', async () => {
        // GIVEN
        const onAddTodoSpy = jest.fn();
        render(<AddTodo onAddTodo={onAddTodoSpy}/>);
        const inputElement = await screen.findByRole("textbox");

        // WHEN
        userEvent.type(inputElement, '   ');
        userEvent.type(inputElement, '{enter}');

        // THEN
        expect(inputElement).toHaveValue('   ');
        expect(onAddTodoSpy).not.toHaveBeenCalled();
    });

    it('should notify parent with trim title when edited title valid and user pressed enter', async () => {
        // GIVEN
        const onAddTodoSpy = jest.fn();
        render(<AddTodo onAddTodo={onAddTodoSpy}/>);
        const inputElement = await screen.findByRole("textbox");

        // WHEN
        userEvent.type(inputElement, "Buy Unicorn ");
        userEvent.type(inputElement, '{enter}');

        // THEN
        expect(inputElement).toHaveValue("");
        expect(onAddTodoSpy).toHaveBeenCalledTimes(1);
        expect(onAddTodoSpy).toHaveBeenCalledWith("Buy Unicorn");
    });
});