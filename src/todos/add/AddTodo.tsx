import React, {ChangeEvent, KeyboardEvent, useState} from "react";

type AddTodoProps = {
    onAddTodo: (title: string) => void
};

const AddTodo = ({onAddTodo}: AddTodoProps) => {

    const [todoTitle, setTodoTitle] = useState("");

    const handleKeyUp = (event: KeyboardEvent) => {
        const cleanTodoTitle = todoTitle.trim();
        if (event.key === "Enter" && cleanTodoTitle) {
            onAddTodo(cleanTodoTitle);
            setTodoTitle("");
        }
    }

    return (
        <input className="new-todo"
               placeholder="What needs to be done?"
               autoFocus
               value={todoTitle}
               onChange={(event: ChangeEvent<HTMLInputElement>) => setTodoTitle(event.target.value)}
               onKeyUp={handleKeyUp}/>
    );
}

export default AddTodo;