import {Todos} from "./todo";
import {useEffect, useState} from "react";

const TODOS_KEY = "todos";

function getStoredTodos(initialTodos: Todos): Todos {
    const storedTodos = localStorage.getItem(TODOS_KEY);
    return storedTodos ? JSON.parse(storedTodos) : initialTodos;
}

const useTodosStorage = (initialTodos: Todos) => {
    const [todos, setTodos] = useState<Todos>(() => getStoredTodos(initialTodos));

    useEffect(() => {
        localStorage.setItem(TODOS_KEY, JSON.stringify(todos));
    }, [todos]);

    return {todos, setTodos};
};

export default useTodosStorage;