import React from 'react';
import {Link} from "react-router-dom";
import {Filter} from "../filter";

type FooterProps = {
    activeCount: number,
    onClearCompleted: () => void,
    onUpdateFilter: (filter: Filter) => void
};

const Footer = ({activeCount, onClearCompleted, onUpdateFilter}: FooterProps) => {
    return (
        <footer className="footer">
            <span
                className="todo-count"><strong>{activeCount}</strong>{activeCount === 1 ? ' item' : ' items'} left</span>
            <ul className="filters">
                <li>
                    <Link to="/" onClick={() => onUpdateFilter(Filter.ALL)}>All</Link>
                </li>
                <li>
                    <Link to="/active" onClick={() => onUpdateFilter(Filter.ACTIVE)}>Active</Link>
                </li>
                <li>
                    <Link to="/completed" onClick={() => onUpdateFilter(Filter.COMPLETED)}>Completed</Link>
                </li>
            </ul>
            <button className="clear-completed" onClick={() => onClearCompleted()}>Clear completed</button>
        </footer>
    );
}

export default Footer;