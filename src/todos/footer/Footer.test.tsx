import React from 'react';
import {render, screen} from '@testing-library/react';
import Footer from "./Footer";
import {HashRouter} from "react-router-dom";
import {Filter} from "../filter";
import userEvent from "@testing-library/user-event";

describe('Footer component', () => {

    const setUp = (content: JSX.Element) => <HashRouter>{content}</HashRouter>;

    it('should render properly', async () => {
        // GIVEN
        const {asFragment} = render(setUp(<Footer activeCount={3} onUpdateFilter={jest.fn()}
                                                  onClearCompleted={jest.fn()}/>));
        const filterList = await screen.findByRole("list");

        // THEN
        expect(asFragment()).toMatchSnapshot();
        expect(filterList).toBeInTheDocument();
    });

    it.each`
  filter                | filterText
  ${Filter.ALL}         | ${'All'}
  ${Filter.ACTIVE}      | ${'Active'}
  ${Filter.COMPLETED}   | ${'Completed'}
`('should notify parent with filter "$filterText" when clicked', async ({filter, filterText}) => {
        // GIVEN
        const updateFilterSpy = jest.fn();
        render(setUp(<Footer activeCount={3} onUpdateFilter={updateFilterSpy}
                             onClearCompleted={jest.fn()}/>));
        const filterElement = await screen.findByRole("link", {name: filterText});

        // WHEN
        userEvent.click(filterElement);

        // THEN
        expect(filterElement).toHaveTextContent(filterText);
        expect(updateFilterSpy).toHaveBeenCalledTimes(1);
        expect(updateFilterSpy).toHaveBeenCalledWith(filter);
    });

    it('should notify parent when "Clear Completed" is clicked', async () => {
        // GIVEN
        const onClearCompleted = jest.fn();
        render(setUp(<Footer activeCount={3} onUpdateFilter={jest.fn()}
                             onClearCompleted={onClearCompleted}/>));

        const clearCompletedButton = await screen.findByRole('button');
        expect(clearCompletedButton).toBeInTheDocument();

        // WHEN
        userEvent.click(clearCompletedButton);

        // THEN
        expect(onClearCompleted).toHaveBeenCalledTimes(1);
        expect(onClearCompleted).toHaveBeenCalledWith();
    });
});