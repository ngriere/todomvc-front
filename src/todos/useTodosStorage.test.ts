import {renderHook} from "@testing-library/react-hooks";
import useTodosStorage from "./useTodosStorage";
import {v4 as uuidv4} from 'uuid';
import {act} from "react-dom/test-utils";

describe('useTodosStorage hook', () => {

    beforeEach(() => {
        localStorage.clear();
    });

    it('should initialise properly if no todos in storage', async () => {
        // GIVEN
        const {result} = renderHook(() => useTodosStorage([]));

        // THEN
        expect(result.current.todos.length).toBe(0);
        expect(result.current.todos).toEqual([]);
    });

    it('should initialise properly if todos in storage', async () => {
        // GIVEN
        const initialTodos = [{
            id: uuidv4(),
            title: "Buy Unicorn",
            completed: false,
            order: 1
        }];
        const {result} = renderHook(() => useTodosStorage(initialTodos));

        // THEN
        expect(result.current.todos.length).toBe(1);
        expect(result.current.todos).toEqual(initialTodos);
    });

    it('should update storage with todos', async () => {
        // GIVEN
        const initialTodos = [{
            id: uuidv4(),
            title: "Buy Unicorn",
            completed: false,
            order: 1
        }];
        const {result} = renderHook(() => useTodosStorage(initialTodos));

        // WHEN
        const updatedTodos = [
            {
                id: uuidv4(),
                title: "Buy Unicorn",
                completed: true,
                order: 1
            },
            {
                id: uuidv4(),
                title: "Do Laundry",
                completed: false,
                order: 2
            }
        ];
        await act(async () => result.current.setTodos(updatedTodos));

        // THEN
        expect(result.current.todos.length).toBe(2);
        expect(result.current.todos).toEqual(updatedTodos);
    });
});
