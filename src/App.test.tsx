import React from 'react';
import {render, screen} from '@testing-library/react';
import App from './App';
import {HashRouter} from "react-router-dom";

describe('App component', () => {

    const setUp = (content: JSX.Element) => <HashRouter>{content}</HashRouter>;

    it('should render properly', async () => {
        render(setUp(<App/>));
        const linkElement = screen.getByText(/todos/i);
        expect(linkElement).toBeInTheDocument();
    });
});
